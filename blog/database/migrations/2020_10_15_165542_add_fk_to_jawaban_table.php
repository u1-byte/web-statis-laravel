<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkToJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jawaban', function (Blueprint $table) {
            //
            $table->unsignedInteger('pertanyaan_id');

            $table->foreign('pertanyaan_id')
            ->references('id')->on('pertanyaan')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->unsignedInteger('profil_id');

            $table->foreign('profil_id')
            ->references('id')->on('profil')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jawaban', function (Blueprint $table) {
            //
            $table->dropForeign(['pertanyaan_id']);
            $table->dropColumn(['pertanyaan_id']);

            $table->dropForeign(['profil_id']);
            $table->dropColumn(['profil_id']);
        });
    }
}
