@extends('admin.master')

@section('content')
<div class="m-4">
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Buat Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan" method="POST">
            @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="judul">Judul</label>
              <input type="text" class="form-control" id="judul" name="judul" placeholder="Masukkan judul">
            </div>
            <div class="form-group">
              <label for="isi">Isi</label>
              <input type="text" class="form-control" id="isi" name="isi" placeholder="Masukkan isi">
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
    </div>
</div>
@endsection