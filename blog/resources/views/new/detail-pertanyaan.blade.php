@extends('admin.master')

@section('content')
<div class="m-4">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">{{ $data->judul }}</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
            <span class="description"> Dibuat pada : {{ $data->tanggal_dibuat }} </span> <br>
            <span class="description"> Terakhir diupdate pada : {{ $data->tanggal_diperbaharui }} </span> <br> <br>
            <p> {{ $data->isi }} </p>
        </div>
        <!-- /.card-body -->
    </div>
</div>
    
@endsection