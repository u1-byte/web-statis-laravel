@extends('admin.master')

@section('content')
    <div class="m-4">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">List Pertanyaan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              @if(session('success'))
                <div class="alert alert-success">
                  {{ session('success') }}
                </div>
              @endif

              <table id="example1" class="table table-bordered table-striped">

                <thead>
                <tr>
                  <th>No</th>
                  <th>Judul</th>
                  <th>Pertanyaan</th>
                  <th>Action</th>
                </tr>
                </thead>

                <tbody>
                  @foreach ($data as $key => $value)
                      <tr>
                        <td> {{ $key + 1 }} </td>
                        <td> {{ $value->judul }} </td>
                        <td> {{ $value->isi }} </td>
                        <td class="project-actions text-center">
                          <a class="btn btn-primary btn-sm" href="/pertanyaan/{{ $value->id }}">
                              <i class="fas fa-folder">
                              </i>
                              SHOW
                          </a>
                          <a class="btn btn-info btn-sm" href="/pertanyaan/{{ $value->id }}/edit">
                              <i class="fas fa-pencil-alt">
                              </i>
                              EDIT
                          </a>
                          <form action="/pertanyaan/{{ $value->id }}/delete" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" value="delete" class="btn btn-danger btn-sm">
                              <i class="fas fa-trash">
                              </i>
                              DELETE
                            </button>
                          </form>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
                
              </table>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
@endsection