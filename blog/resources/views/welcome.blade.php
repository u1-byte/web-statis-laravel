<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Selamat Datang!</title>
</head>
<body>
    <h2>SELAMAT DATANG {{ $fname }} {{ $lname }}!</h2>
    <h3>
        Terima kasih telah bergabung di SanberBook. 
        Social Media kita bersama!
    </h3>
    <p>Berikut identitas Anda : 
        <br> Nama : {{ $fname }} {{ $lname }}
        <br> Gender : {{ $gender }}
        <br> Kewarganegaraan : {{ $nation }}
    </p>
</body>
</html>