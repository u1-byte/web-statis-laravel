<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buat Account Baru!</title>
</head>
<body>
    <h2>Buat Account Baru!</h2>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        <!-- First & Last Name -->
        @csrf
        <label for="fname">First Name :</label><br>
        <input type="text" id="fname" name="fname"><br>
        <label for="lname">Last Name :</label><br>
        <input type="text" id="lname" name="lname"><br>
        <br><br>

        <!-- Gender -->
        <label for="gender">Gender :</label><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="othergen" name="gender" value="othergen">
        <label for="othergen">Other</label><br>
        <br><br>
    
        <!-- Nationality -->
        <label for="nation">Nationality :</label>
        <select name="nation" id="nation">
            <option value="Singapore">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Indonesian" selected>Indonesian</option>
            <option value="Australian">Australian</option>
        </select>
        <br><br>

        <!-- Language -->
        <label for="lang">Language Spoken :</label><br>
        <input type="checkbox" id="bahasa" name="lang" value="bahasa">
        <label for="bahasa">Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" name="lang" value="english">
        <label for="english">English</label><br>
        <input type="checkbox" id="otherlang" name="lang" value="otherlang">
        <label for="otherlang">Other</label><br>
        <br><br>

        <!-- Bio -->
        <label for="bio">Bio :</label><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        <br><br>

        <!-- Sign Up Button -->
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>