<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    //
    //public $timestamps = false;
    protected $table = 'pertanyaan';
    const CREATED_AT = 'tanggal_dibuat';
    const UPDATED_AT = 'tanggal_diperbaharui';
    protected $guarded = ['jawaban_tepat_id', 'profil_id'];
}
