<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    //
    public function index(){
        $data = Pertanyaan::all();
        //$data = DB::table('pertanyaan')->get();
        return view('new.lists-pertanyaan', compact('data'));
    }

    public function create(){
        return view('new.create-pertanyaan');
    }

    public function store(Request $request){
        
        $title = $request['judul'];
        $content = $request['isi'];
        $date = Carbon::now();

        /*
        $query = DB::table('pertanyaan')->insert([
            'judul' => $title,
            'isi' => $content,
            'tanggal_dibuat' => $date,
            'tanggal_diperbaharui' => $date
            ]
        );*/

        $data = new Pertanyaan;
        $data->judul = $title;
        $data->isi = $content;
        $data->tanggal_dibuat = $date;
        $data->tanggal_diperbaharui = $date;
        $data->save();

        return redirect('/pertanyaan')->with('success', 'Data baru berhasil ditambahkan!');
    }

    public function show($id){
        //$data = DB::table('pertanyaan')->where('id', $id)->first();
        $data = Pertanyaan::find($id);
        return view('new.detail-pertanyaan', compact('data'));
    }

    public function edit($id){
        //$data = DB::table('pertanyaan')->where('id', $id)->first();
        $data = Pertanyaan::find($id);
        return view('new.edit-pertanyaan', compact('data'));
    }

    public function update($id, Request $request){
        $title = $request['judul'];
        $content = $request['isi'];
        $date = Carbon::now();

        /*$query = DB::table('pertanyaan')->where('id', $id)->update([
            'judul' => $title,
            'isi' => $content,
            'tanggal_diperbaharui' => $date
            ]
        );*/

        $data = Pertanyaan::find($id);
        $data->judul = $title;
        $data->isi = $content;
        $data->tanggal_diperbaharui = $date;
        $data->save();

        return redirect('/pertanyaan')->with('success', 'Data berhasil diperbaharui!');
    }

    public function destroy($id){
        //$data = DB::table('pertanyaan')->where('id', $id)->delete();
        $data = Pertanyaan::find($id);
        $data->delete();
        return redirect('/pertanyaan')->with('success', 'Data berhasil dihapus!');
    }

}
