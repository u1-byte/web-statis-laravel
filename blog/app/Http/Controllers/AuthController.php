<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register() {
        return view('form');
    }

    public function welcome(Request $req) {
        $first = $req['fname'];
        $last = $req['lname'];
        $gen = $req['gender'];
        $nat = $req['nation'];
        if($gen == 'male'){
            $gen = 'laki - laki';
        }
        elseif($gen == 'female'){
            $gen = 'perempuan';
        }
        elseif($gen == 'othergen'){
            $gen = '***';
        }
        $res = ['fname' => $first, 'lname' => $last, 'gender' => $gen, 'nation' => $nat];
        return view('welcome', $res);
    }
}
